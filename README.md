# README #

This is a gradle project. 
To run it, cd to root of project and do

```
#!scala

./gradlew cleanTest test -i
```

The desired result shows in the stdout of the last test executed:


```
#!scala

LibrarySuite > three longest unique (exists once within src string) palindromes in example given STANDARD_OUT
    Text: hijkllkjih, Index: 23, Length: 10
    Text: ijkllkji, Index: 24, Length: 8
    Text: defggfed, Index: 13, Length: 8
```

**NOTE**

I implemented the task with "unique" meaning that the palindrome only occurs once in the string. That is why 'ijkllkji' shows in the output above others because it is among the 3 longest AND it only occurs once within the string.
After looking at your output, it is clear that you mean by "unique" not "only once in string" but "not part of another palindrome". I can change the code accordingly if needed.