import scala.annotation.tailrec
import scala.util.Random
import scala.collection.mutable.MutableList

class Library {
  def someLibraryMethod(): Boolean = {
    true
  }
  @tailrec
  final def isPalindrome(s: String): Boolean =
    if (s.size <= 1) true else s.head == s.reverse.head && isPalindrome(s.tail.dropRight(1))

  def collectPalindromesOfSize(s: String, n: Int) = {
    val result = new scala.collection.mutable.HashMap[String, Int]()
    if (n < 1) result else if (n == 1) {

      s.map { x => result.+=((x.toString, result.getOrElse(x.toString, 0) + 1)) }

      result
    } else {

      val paldromes = allSubstringsOfSize(s, n).filter { subs => isPalindrome(subs) }
      paldromes.map { paldrome => result.+=((paldrome, result.getOrElse(paldrome, 0) + 1)) }
      result
    }

  }

  def allSubstringsOfSize(s: String, n: Int) = {
    for (i <- 0 to s.length - n) yield s.substring(i, Math.min(i + n, s.length))
  }

  def collectUniquePalindromesOfSize(s: String, n: Int) = collectPalindromesOfSize(s, n).filter { pc: ((String, Int)) => pc._2 == 1 }.keys.toList

  def collectAtleast3UniquePalindromesWithDecreasingLength(
    top3palindromes: MutableList[String], s: String, startlength: Int): MutableList[String] = {
    top3palindromes.++=(collectUniquePalindromesOfSize(s, startlength))
    if (top3palindromes.size < 3 && startlength > 0) collectAtleast3UniquePalindromesWithDecreasingLength(top3palindromes, s, startlength - 1)
    else top3palindromes

  }

}
